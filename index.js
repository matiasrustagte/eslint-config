module.exports = {
  env: {
    es6: true,
    node: true,
  },
  extends: 'airbnb-base',
  globals: {
      Atomics: 'readonly',
      SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
  },
  rules: {
    'max-len': [
      2,
      {
        code: 120,
        comments: 120,
        tabWidth: 2,
      },
    ],
    'space-before-function-paren': [2, 'always'],
  },
};
